FORMAT: 1A
HOST: http://test.travelclub.yalantis.com

# travel_club


# Group Users
Doc about **Users API**

## Users Collection [/api/users]
### List all Users [GET]
+ Response 200 (application/json)

    + Body   
    
            [
              {
                name: "Валерий Рябов"
                fb_token: "CAACEdEose0cBACXnthaiQmx8WZAS6kvmSxLSfntZC06KiL4vnMwtu1LrmQPUuHcZAOzRVfNaE8CVFn9ZCRwpM7O5dSQeVTfx3XJhSKdk7XIZBkJctTx2G0fqOr1KhfFctDDVxpwS4WcOZCNmwe5Cd5ElxtcLV2v5Q6DPAzboZAZAh7OIsuKcW7itppR2kxSVtXh6SBVyLiwMYQZDZD",
                fb_id: "1393600642"
                id: 1
              }
            ]

### Create User [POST]
+ Request (application/json)
    
    + Body
    
            {
              "fb_token":"CAACEdEose0cBAE3GV8yZBixwXXwZCfdhAK9XOW3KfXHGirHRUm9r0xX9lXSzjGozL49wBRSSIrObLEii5lfKddfvLklJ9gLocwLq32KPn2zUmJW4rstFc5L8FX9pMK4gVJ2cZBh3JSmjphGdCOyoEEDKuBnU9VZCN1EdyhCEclzkLA2AcRA9FRmSrcGUmukSAxvQufhUhQZDZD",
              "fb_id":"1393600642"
            }
    
    + Schema

            {
                "type": "object",
                "required": true,
                "properties": {
                    "fb_token": {
                        "type": "string",
                        "required": true
                    },
                    "fb_id": {
                        "type": "string",
                        "required": true
                    }
                }
            }

+ Response 201 (application/json)

        {
            "friends": [
                {
                    "name": "Eugene Marchenko",
                    "id": "100001605906792"
                },
                {
                    "name": "Константин Рогачев",
                    "id": "100001667758826"
                },
                {
                    "name": "Vitaliy  Dudinkin",
                    "id": "100002350276985"
                },
                {
                    "name": "Bogdan  Martynenko",
                    "id": "100002787844469"
                }
            ],
            "name": "Валерий Рябов",
            "fb_id":"1393600642",
            "fb_token": "CAACEdEose0cBAE3GV8yZBixwXXwZCfdhAK9XOW3KfXHGirHRUm9r0xX9lXSzjGozL49wBRSSIrObLEii5lfKddfvLklJ9gLocwLq32KPn2zUmJW4rstFc5L8FX9pMK4gVJ2cZBh3JSmjphGdCOyoEEDKuBnU9VZCN1EdyhCEclzkLA2AcRA9FRmSrcGUmukSAxvQufhUhQZDZD",
            "avatar_small": "http://graph.facebook.com/1393600642/picture",
            "id": 4
        }    

## Friends Collection [/api/friends]
### Send invite to friends [POST]
+ Request (application/json)

        {
          "friends":["100001605906792", "100001667758826", "100002350276985", "100002787844469"]
        }

+ Response 200 (application/json)

# Group Events

## Events Collection [/api/events{?user_id,page}]
### List all Events [GET]

+ Parameters
    + user_id (required, number, `1`) ... Numeric `id` of the User
    + page (required, number, `1`) ... Number of the page

+ Response 200 (application/json)

        [
            {
                name: "FirstTravel"
                descr: "Some description"
                date_from: "2015-07-14T00:00:00"
                image_id: 14
                date_to: "2015-07-18T00:00:00"
                id: 1
                owner_id: 2
            },
            {
                name: "FirstTravel"
                descr: "Some description"
                date_from: "2015-07-14T00:00:00"
                image_id: 15
                date_to: "2015-07-18T00:00:00"
                id: 2
                owner_id: 2
            }
        ]

## Events Creation [/api/events]
### Create an Event [POST]
+ Request (application/json)
    
    + Body
    
            {
                "date_from": "2015-07-14", 
                "owner_id": 2, 
                "name": "FirstTravel", 
                "descr": "Some description", 
                "date_to": "2015-07-18"
            }
    
    + Schema

            {
                "type": "object",
                "required": true,
                "properties": {
                    "date_from": {
                        "type": "datetime" ("2014-05-07T09:33:57"),
                        "required": true
                    },
                    "owner_id": {
                        "type": "number",
                        "required": true
                    },
                    "name": {
                        "type": "string",
                        "required": true
                    },
                    "descr": {
                        "type": "string",
                        "required": false
                    },
                    "date_to": {
                        "type": "datetime" ("2014-05-07T09:33:57"),
                        "required": true
                    }
                }
            } 

+ Response 201 (application/json)

        {
            "name": "FirstTravel", 
            "descr": "Some description", 
            "date_from": "2015-07-14", 
            "date_to": "2015-07-18",
            "image_id": 9, 
            "owner_id": 2
        }  


## Events single actions [/api/events/{id}]
A single Event object with all its details

+ Parameters
    + id (required, number, `1`) ... Numeric `id` of the Event

### Get single Event [GET]
+ Response 200 (application/json)

        {
            "name": "Big Trip",
            "descr": "trip for all friends",
            "date_from": "2015-06-14T00:00:00",
            "image_id": 23,
            "prs_issues": [
                {
                    "status": "false",
                    "user_id": 3,
                    "name": "Get sword",
                    "id": 5
                }
            ],
            "members": [
                {
                    "id": 2,
                    "name": "Валерий Рябов",
                    "avatar_small": "http://graph.facebook.com/1393600642/picture"
                }
            ],
            "date_to": "2015-06-20T00:00:00",
            "cmn_issues": [
                {
                    "status": "false",
                    "name": "Get booz",
                    "id": 3
                }
            ],
            "img_url": "http://127.0.0.1:5000/api/img/23",
            "id": 6,
            "owner_id": 2
        }

### Update single Event [PUT]
+ Request (application/json)

    + Body

            {
                "date_from": "2015-07-14", 
                "name": "FirstTravel", 
                "descr": "Some description", 
                "date_to": "2015-07-18"
            }

    + Schema

            {
                "type": "object",
                "required": true,
                "properties": {
                    "date_from": {
                        "type": "datetime" ("2014-05-07T09:33:57"),
                        "required": false
                    },
                    "date_to": {
                        "type": "datetime" ("2014-05-07T09:33:57"),
                        "required": false
                    },
                    "descr": {
                        "type": "string",
                        "required": false
                    },
                    "name": {
                        "type": "string",
                        "required": false
                    }
                }
            }       

+ Response 201 (application/json)
        
        {
            name: "FirstTravel",
            descr: "Some description",
            date_from: "2015-06-14T00:00:00",
            image_id: 22,
            date_to: "2015-07-18T00:00:00",
            img_url: "http://127.0.0.1:5000/api/img/22",
            id: 1,
            owner_id: 2
        }

## Event join [/api/join_event]
### Join User to Event [POST]
+ Request (application/json)

    + Body

            {
              "user_id":6,
              "event_id" : 2,
            }

    + Schema

            {
                "type": "object",
                "required": true,
                "properties": {
                    "user_id": {
                        "type": "number",
                        "required": true
                    },
                    "event_id": {
                        "type": "number",
                        "required": true
                    }
                }
            }       

+ Response 200 (application/json)

## Event leave [/api/leave_event]
### User leaves Event [POST]
+ Request (application/json)

    + Body

            {
              "user_id":6,
              "event_id" : 2,
            }

    + Schema

            {
                "type": "object",
                "required": true,
                "properties": {
                    "user_id": {
                        "type": "number",
                        "required": true
                    },
                    "event_id": {
                        "type": "number",
                        "required": true
                    }
                }
            }

+ Response 200 (application/json)

# Group Issues
## Collection issues [/api/issues/]
### Get all issues [GET]

+ Response 200 (application/json)

        {
            "personal_issues": [
                {
                    "status": "false",
                    "event_id": 6,
                    "user_id": 3,
                    "name": "Get sword",
                    "id": 5
                }
            ],
            "common_issues": [
                {
                    "status": "false",
                    "event_id": 6,
                    "name": "Get booz",
                    "id": 3
                }
            ]
        }

### Create issue [POST]
+ Request (application/json)

    + Body

            {
              "user_id":2,
              "event_id" : 2,
              "name" : "Get ball",
              "status": "false",
              "issue_type": "personal"
            }

    + Schema

            {
                "type": "object",
                "required": true,
                "properties": {
                    "user_id": {
                        "type": "number",
                        "required": true (if issue_type == "personal")
                    },
                    "event_id": {
                        "type": "number",
                        "required": true
                    },
                    "name": {
                        "type": "number",
                        "required": true
                    },
                    "status": {
                        "type": "string",
                        "required": true
                    },
                    "issue_type": {
                        "type": "string" ("personal" or "common"),
                        "required": true
                    }
                }
            }

+ Response 200 (application/json)

        {
            "status": false,
            "event_id": 2,
            "issue_id": 7,
            "user_id": 2
        }
