"""empty message

Revision ID: 49c854087c8f
Revises: None
Create Date: 2014-07-23 16:26:56.107843

"""

# revision identifiers, used by Alembic.
revision = '49c854087c8f'
down_revision = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.create_table('imagesnew',
    sa.Column('name', sa.Unicode(length=64), nullable=True),
    sa.Column('path', sa.Unicode(length=128), nullable=True),
    sa.Column('order_id', sa.Unicode(length=128), nullable=True),
    sa.Column('id', sa.Integer(), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('imagesnew')
    ### end Alembic commands ###
