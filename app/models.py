#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os.path as op
import os

from extensions import db
import config as CFG
from mixin import CRUDMixin, BaseMixin
from utils import as_dict_many

from flask import url_for, request
from sqlalchemy.event import listens_for
from sqlalchemy.ext.hybrid import hybrid_property
from flask.ext.admin import form


user_events = db.Table('user_events',
                       db.Column(
                       'user_id', db.Integer, db.ForeignKey('users.id')),
                       db.Column(
                       'event_id', db.Integer, db.ForeignKey('events.id'))
                       )


class Users(db.Model, CRUDMixin):
    fb_token = db.Column(db.String(256))
    fb_id = db.Column(db.String(256))
    name = db.Column(db.String(256))
    image_id = db.Column(db.Integer, db.ForeignKey('images.id'))
    registration_id = db.Column(db.Unicode(64))

    issues = db.relationship("Issues")

    @hybrid_property
    def avatar(self):
        if self.image_id is None:
            return None
        return request.url_root + url_for('api.img', img_id=self.image_id).strip('/')


class Images(db.Model, CRUDMixin):
    name = db.Column(db.Unicode(64))
    path = db.Column(db.Unicode(128))

    def __unicode__(self):
        return self.name


class Events(db.Model, CRUDMixin):
    name = db.Column(db.String(256))
    descr = db.Column(db.String(256))
    date_from = db.Column(db.DateTime(timezone=False))
    date_to = db.Column(db.DateTime(timezone=False))
    image_id = db.Column(db.Integer, db.ForeignKey('images.id'))
    owner_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    members = db.relationship(Users, secondary=user_events,
                              backref=db.backref('events', lazy='dynamic'))
    all_issues = db.relationship("Issues")
    owner = db.relationship("Users")

    @hybrid_property
    def img_url(self):
        if self.image_id is None:
            return None
        return request.url_root + url_for('api.img', img_id=self.image_id).strip('/')

    @hybrid_property
    def issues(self):
        return as_dict_many(self.all_issues, exclude=["event_id"])

    @hybrid_property
    def cnt_memebers(self):
        return len(list(self.members))


class Issues(db.Model, CRUDMixin):

    __tablename__ = "issues"

    issue_type = db.Column(db.String(256))
    status = db.Column(db.String(256), default=False)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    event_id = db.Column(db.Integer, db.ForeignKey('events.id'))
    issue_group_id = db.Column(db.Integer, db.ForeignKey('issue_groups.id'))

    group = db.relationship("IssueGroups")


class IssueGroups(db.Model, CRUDMixin):

    __tablename__ = "issue_groups"

    name = db.Column(db.String(256))

    def __unicode__(self):
        return self.name


class ImagesNew(db.Model, CRUDMixin):
    name = db.Column(db.Unicode(64))
    path = db.Column(db.Unicode(128))
    order_id = db.Column(db.Unicode(128))

    def __unicode__(self):
        return self.name


@listens_for(Images, 'after_delete')
def del_image(mapper, connection, target):
    if target.name:
        # Delete image
        try:
            os.remove(op.join(CFG.IMG_ROOT, target.name))
        except OSError:
            pass

        # Delete thumbnail
        try:
            os.remove(op.join(CFG.IMG_ROOT,
                              form.thumbgen_filename(target.name)))
        except OSError:
            pass
