#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os.path as op
import sys
from pprint import pprint
import time
import datetime
import json
import facebook
from gcm import GCM

from flask import Response
from werkzeug.utils import secure_filename

import config as CFG


def print_obj(object_name):
    try:
        pprint({x: getattr(object_name, x) for x in dir(object_name)})
    except:
        for x in dir(object_name):
            try:
                print {x: getattr(object_name, x)}
            except:
                'cant print %s' % x


def json_handler(obj):
    if isinstance(obj, datetime.date) or isinstance(obj, datetime.datetime):
        return obj.isoformat()
    else:
        return None


def jsonify(data, status=200):
    """
    Custom jsonify function, supports lists
    """
    timestamp = time.time()
    last_mod = time.strftime(
        '%a, %d %b %Y %H:%M:%S GMT', time.gmtime(timestamp))
    return Response(
        response=json.dumps(data, default=json_handler),
        content_type='application/json',
        status=status,
        headers={
            'If-Modified-Since': last_mod,
            'Cache-Control': True
        }
    )


def as_dict_many(obj_list, **kwargs):
    return map(lambda x: x.as_dict(**kwargs), obj_list)


def as_dict_many_ext(query_obj):
    """Use when session.query() query"""
    return [dict(zip(obj.keys(), obj)) for obj in query_obj]


def filter_get_params(args, filter_list):
    if args:
        return {k: v for k, v in args.iteritems() if k in filter_list}
    else:
        return {}


def get_fb_friends(token):
    graph = facebook.GraphAPI(token)
    friends_fb = graph.get_object("me/friends")["data"]
    for f in friends_fb:
        f.update({"avatar": "http://graph.facebook.com/%s/picture" % f["id"]})
    return friends_fb


def get_fb_name(token):
    graph = facebook.GraphAPI(token)
    user = graph.get_object("me")
    return user["first_name"] + ' ' + user["last_name"]


def valid_fb_token(token):
    try:
        facebook.GraphAPI(token).get_object("me")
        return True
    except:
        return False


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in CFG.ALLOWED_EXTENSIONS


def load_file(file_image):
    try:
        if file_image and allowed_file(file_image.filename):
            filename = secure_filename(file_image.filename)
            file_image.save(op.join(CFG.IMG_ROOT, filename))
    except:
        print "Unexpected error when load file:", sys.exc_info()[0]


def send_push_notification(registration_id, message):
    API_KEY = '1029814691737'
    gcm = GCM(API_KEY)
    resp = gcm.plaintext_request(registration_id=registration_id,
                                 data={'message': message})
