#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os.path as op
import urllib

from flask.views import MethodView
from flask import (Blueprint, request, current_app, send_file, redirect)
from werkzeug.utils import secure_filename

from models import(
    Users, Events, Images, Issues, IssueGroups
)
from utils import (
    jsonify,
    filter_get_params,
    valid_fb_token,
    get_fb_friends,
    get_fb_name,
    as_dict_many,
    print_obj,
    load_file
)
from extensions import db
import config as CFG
from utils import send_push_notification

api = Blueprint('api', 'api')


@api.before_request
def api_br():
    if request.method == 'GET' and request.args:
        current_app.logger.info(request.args)
    if request.method == 'POST' and request.form:
        current_app.logger.info(request.form)
    if request.method == 'POST' and request.json:
        current_app.logger.info(request.json)


class UsersAPI(MethodView):

    def get(self):
        users = as_dict_many(Users.query.all())
        return jsonify(users)

    def post(self):
        allowed_params = ["fb_token", "fb_id"]
        ret_args = filter_get_params(request.json, allowed_params)
        fb_token = ret_args.get("fb_token", None)
        fb_id = ret_args.get("fb_id", None)


        if not valid_fb_token(fb_token):
            return jsonify(
                {"message": "Invalid facebook token"}, status=400)

        user = Users.query.filter_by(fb_id=fb_id).first()

        if user and user.fb_token != fb_token:
            # token expired, need to update
            user.update(fb_token=fb_token)

        friends = get_fb_friends(fb_token)

        if not user:
            url_img = 'http://graph.facebook.com/%s/picture?type=normal' % fb_id
            file_name = fb_id + '.jpeg'
            urllib.urlretrieve(url_img, op.join(CFG.IMG_ROOT, file_name))
            image = Images.create(
                name=file_name,
                path=op.join(CFG.IMG_ROOT, file_name)
            )
            image_id = image.id

            name = get_fb_name(fb_token)
            user = Users.create(fb_token=fb_token, name=name, fb_id=fb_id, image_id=image_id)

        resp = user.as_dict()
        resp.update({"friends": friends})

        return jsonify(resp)


class EventsAPI(MethodView):

    def get(self, event_id):
        if event_id is None:
            PAGE_SIZE = 15
            allowed_params = ["user_id", "page"]
            ret_args = filter_get_params(request.args, allowed_params)

            cond = []
            if "user_id" in ret_args:
                cond.append(Users.id == ret_args["user_id"])

            event_q = (
                Events
                .query
                .join(Users.events)
                .filter(*cond)
            )

            if "page" in ret_args:
                page_number = int(ret_args["page"])
                event_q = event_q.limit(PAGE_SIZE).offset(
                    PAGE_SIZE * (page_number - 1))

            event_q = event_q.all()

            events = as_dict_many(event_q)
            return jsonify(events)
        else:
            event_query = Events.get(event_id)
            event = event_query.as_dict(exclude="image_id")
            event["members"] = as_dict_many(
                event_query.members, exclude=["fb_token", "fb_id"])
            for iss in event["issues"]:
                iss["name"] = IssueGroups.get(iss["issue_group_id"]).name
            return jsonify(event)

    def post(self):
        allowed_params = ["name", "descr", "date_from", "date_to", "owner_id"]
        ret_args = filter_get_params(request.form, allowed_params)

        print_obj(request)
        file_image = request.files.get("pic", None)

        if file_image:
            file_name = secure_filename(file_image.filename)
            
            load_file(file_image)

            image = Images.create(
                name=file_name,
                path=op.join(CFG.IMG_ROOT, file_name)
            )
            ret_args["image_id"] = image.id

        event = Events.create(**ret_args)

        # take participate in event
        user = Users.get(ret_args["owner_id"])
        event.members.append(user)
        db.session.commit()

        return jsonify(event.as_dict())

    def put(self, event_id):
        allowed_params = ["name", "descr", "date_from", "date_to"]
        ret_args = filter_get_params(request.form, allowed_params)
        file_image = request.files.get("pic", None)

        if file_image:
            file_name = secure_filename(file_image.filename)
            try:
                load_file(file_image)
            except:
                return jsonify({"error": "cant load file"})

            image = Images.create(
                name=file_name,
                path=op.join(CFG.IMG_ROOT, file_name)
            )
            ret_args["image_id"] = image.id

        event = Events.get(event_id)
        old_image_id = event.image_id
        event = event.update(**ret_args).as_dict()
        if "image_id" in ret_args:
            Images.get(old_image_id).delete()

        return jsonify(event)


class IssuesAPI(MethodView):

    def get(self):

        # allowed_params = ["user_id", "event_id"]
        # ret_args = filter_get_params(request.args, allowed_params)
        issues = as_dict_many(Issues.query.all())
        return jsonify(issues)

    def post(self):

        allowed_params = [
            "user_id", "event_id", "name", "status", "issue_type"]
        ret_args = filter_get_params(request.json, allowed_params)

        if "issue_type" not in ret_args:
            return jsonify({"error": "issue_type parameter required"}, 403)

        if ret_args["issue_type"] not in ["common", "personal"]:
            return jsonify({"error": "Unknown issue_type"}, 403)

        user = Users.get(ret_args["user_id"])
        event = Events.get(ret_args["event_id"])

        if ret_args["issue_type"] == "personal" and "user_id" in ret_args:
            ret_args.pop("user_id")

        if user is not event.owner:
            return jsonify({"error":"You can't create issue"}, 403)

        issue_group = IssueGroups.create(name=ret_args.pop("name"))

        
        issue = Issues.create(**ret_args)
        issue.group = issue_group

        if ret_args["issue_type"] == "common":
            for u in (set(event.members)-set([user])):
               ret_args["user_id"] = u.id
               i = Issues.create(**ret_args)
               i.group = issue_group      

        db.session.commit()

        final_iss = issue.as_dict(exclude=["issue_group_id"])
        final_iss["group"] = issue_group.as_dict()

        return jsonify(final_iss)

    def put(self, issue_id):
        allowed_params = ["status", "issue_type", "user_id"]
        ret_args = filter_get_params(request.json, allowed_params)
        
        issue_type = ret_args.pop("issue_type")
        issue = Issues.get(issue_id)

        if not issue:
            return jsonify({"error": "Issue not found"}, 404)


        if issue_type != "personal":
            ret_args.pop("user_id")


        issue.update(**ret_args)

        return jsonify(issue.as_dict())


@api.route('/img/<int:img_id>', methods=['GET'])
def img(img_id):
    image = Images.get(img_id)
    return send_file(image.path)


@api.route('/friends', methods=['POST'])
def friends():
    allowed_params = ["friends", "event_id", "user_id"]
    ret_args = filter_get_params(request.json, allowed_params)

    sender = Users.get(ret_args["user_id"])

    reg_users = Users.query.filter(Users.fb_id.in_(ret_args["friends"])).all()
    reg_users_ids = set([u.fb_id for u in reg_users])
    not_reg_users = set(ret_args["friends"]) - reg_users_ids
    
    for user in reg_users:
        send_push_notification(user.registration_id, "New event!")

    for user_fb_id in not_reg_users:
        pass
        # push_fb(sender.token, user_fb_id, "Message")

    return 'ok'


@api.route('/join_event', methods=["POST"])
def join_event():
    allowed_params = ["user_id", "event_id"]
    ret_args = filter_get_params(request.json, allowed_params)

    if "user_id" not in ret_args or "event_id" not in ret_args:
        return jsonify('Error! user_id and event_id must be specified', 403)

    user = Users.get(ret_args["user_id"])
    event = Events.get(ret_args["event_id"])
    event.members.append(user)

    #common issues mass add
    groups = set([iss["issue_group_id"] for iss in event.issues if iss["issue_type"]=="common"])
    for group in groups:
        Issues.create(issue_group_id = group, issue_type="common", **ret_args)
    db.session.commit()

    return 'ok'


@api.route('/leave_event', methods=["POST"])
def leave_event():
    allowed_params = ["user_id", "event_id"]
    ret_args = filter_get_params(request.json, allowed_params)

    if "user_id" not in ret_args or "event_id" not in ret_args:
        return jsonify('Error! user_id and event_id must be specified', 403)

    user = Users.get(ret_args["user_id"])
    event = Events.get(ret_args["event_id"])

    if user is event.owner:
        return jsonify({"error":"You can't leave this event"}, 403)
    event.members.remove(user)

    for iss in user.issues:
        iss.delete()

    db.session.commit()
    return 'ok'


@api.route('/update_registration/<int:user_id>', methods=["POST"])
def update_registration(user_id):

    allowed_params = ["registration_id"]
    ret_args = filter_get_params(request.json, allowed_params)

    Users.get(user_id).update(registration_id = ret_args["registration_id"])

    return 'ok'


users_view = UsersAPI.as_view('users')
api.add_url_rule('/users', view_func=users_view, methods=["GET", "POST"])

issues_view = IssuesAPI.as_view('issues')
api.add_url_rule('/issues', view_func=issues_view,methods=["GET", "POST"])
api.add_url_rule('/issues/<int:issue_id>', view_func=issues_view, methods=["PUT"])

event_view = EventsAPI.as_view('events')
api.add_url_rule('/events', view_func=event_view,
                 defaults={'event_id': None}, methods=["GET", "PUT"])
api.add_url_rule('/events', view_func=event_view, methods=["POST"])
api.add_url_rule('/events/<int:event_id>', view_func=event_view)
