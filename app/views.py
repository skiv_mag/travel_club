#!/usr/bin/env python
# -*- coding: utf-8 -*-

from models import *
from utils import print_obj
from flask import Blueprint, redirect, request, Response, render_template
from flask.ext.restless.helpers import get_or_create

common = Blueprint('common_page', 'common_page')


@common.route('/', methods=['GET'])
def api():
    return "Blueprint works"


@common.route('/about')
def about():
    return render_template("about.html")
