#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.admin import Admin
from flask.ext.migrate import Migrate
from flask.ext.script import Manager

db = SQLAlchemy()
admin = Admin()
migrate = Migrate()
manager = Manager()