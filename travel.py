#!/usr/bin/env python
# -*- coding: utf-8 -*-

from app import app, db, manager

if __name__ == '__main__':
    manager.run()
